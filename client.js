function Client()
{
    this.game = new Phaser.Game(800, 600, Phaser.AUTO, "new game");
}



Client.prototype.init = function()
{
    this.game.state.add("game_state", GameState);
    this.game.state.add("menu_state", MenuState);

    this.game.state.start("game_state");
}


