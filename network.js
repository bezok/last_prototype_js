


function Network(world)
{
    this.packets = [];
    this.websocket = new WebSocket("ws://localhost:53011");

    this.on_message(this);
    this.world = world;
}



Network.prototype.handle_packet = function()
{
    var length = this.packets.length
    if (length)
    {
        if (new Date().getTime()*1000 - this.packets[0].timeStamp >= 200*1000)
        {
            var packet = this.packets.shift();
            var data = JSON.parse(packet.data);

            if (data["a"])
            {
                this.handle_packet_actions(data["a"]);
            }

            this.world.update_entities(data["u"]);
        }
    }
}



Network.prototype.handle_packet_actions = function(data)
{
    var length = data.length;
    for(var i=0; i<length; i++)
    {
        if (data[i]["a"] === "dc")
        {
            this.world.delete_entity(data[i]["id"]);
        }
        else if (data[i]["a"] === "cc")
        {
            this.world.add_entity(data[i]);
        }
        else if (data[i]["a"] === "sei")
        {
            this.world.entity_id = data[i]["id"];
        }
    }
}



Network.prototype.on_message = function()
{
    var self = this;
    this.websocket.onmessage = function(event)
    {
        console.log(event.data);
        self.packets.push(event);
    }
}



Network.prototype.receive = function()
{

}



Network.prototype.send = function(packet)
{
    this.websocket.send(packet);
}



