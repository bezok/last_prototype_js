


function GameState()
{
    this.entity_id = null;
    this.entities = {};
    this.date = new Date();
    this.network = new Network(this);
}



GameState.prototype.add_entity = function(entity)
{
    this.entities[entity.id] = new Entity(this.game, entity);
}



GameState.prototype.create = function()
{
    this.game.canvas.oncontextmenu = function(e) { e.preventDefault(); }
    this.input = new Input(this.game)
    this.game.camera.bounds = null;
}



GameState.prototype.delete_entity = function(id)
{
    if (this.entities[id])
    {
        this.entities[id].sprite.destroy();
        delete this.entities[id];
    }
}



GameState.prototype.init = function()
{

}



GameState.prototype.update = function()
{
    this.network.handle_packet();
    this.input.handle_input();
}



GameState.prototype.update_entities = function(entities)
{
    if (entities)
    {
        var length = entities.length;

        for (var i=0; i<length; i++)
        {
            if(this.entities[entities[i].id])
            {
                this.entities[entities[i].id].update(entities[i]);
            }
            else
            {
                this.add_entity(entities[i]);
            }
        }
    }
}







