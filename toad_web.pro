TEMPLATE = app
QT = core
CONFIG -= app_bundle
CONFIG += c++11

SOURCES +=

DISTFILES += \
    index.html \
    client.js \
    entity.js \
    network.js \
    gamestate.js \
    menustate.js \
    input.js
