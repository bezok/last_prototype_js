function Entity(game, entity)
{
    this.id = entity.id;
    this.entity_type = entity.entity_type;
    this.x = entity.x/0.02;
    this.y = entity.y/0.02;
    this.angle = entity.angle;
    this.size_x = entity.s_x;
    this.size_y = entity.s_y;

    this.game = game;

    this.sprite = this.create_sprite();
    this.sprite.inputEnabled = true;
}



Entity.prototype.create_sprite = function()
{
    var graphics = this.game.add.graphics(this.x, this.y);
    graphics.lineStyle(2, 0x0000FF, 1);
    graphics.drawRect(-32, -32, 32, 32);
    return this.game.add.sprite(this.x, this.y, graphics.generateTexture());
}



Entity.prototype.move = function()
{
    this.sprite.x = this.x;
    this.sprite.y = this.y;
}



Entity.prototype.update = function(entity)
{
    this.x = entity.x/0.02;
    this.y = entity.y/0.02;
    this.angle = entity.angle;

    this.move();
}
