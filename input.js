


function Input(game)
{
    this.game = game;
    this.keyboard_cursor = game.input.keyboard.createCursorKeys();
    this.pointer = game.input.activePointer;
    this.input_packet = [];
}



Input.prototype.handle_input = function()
{
    this.handle_camera();
}



Input.prototype.handle_camera = function()
{
    if (this.keyboard_cursor.up.isDown)
    {
        this.game.camera.y -= 10;
    }
    else if (this.keyboard_cursor.down.isDown)
    {
        this.game.camera.y += 10;
    }

    if (this.keyboard_cursor.left.isDown)
    {
        this.game.camera.x -= 10;
    }
    else if (this.keyboard_cursor.right.isDown)
    {
        this.game.camera.x += 10;
    }
}



Input.prototype.handle_mouse = function()
{
    this.pointer.onDown = function()
    {

    }
}
